import axios from 'axios';

export default {
  getAdminTeamLeader() {
    // return axios.get('/admin/view-team-leader');
    return axios.get('https://anapioficeandfire.com/api/characters/54', {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
      },
    });
    //  urn axios.get('https://anapioficeandfire.com/api/characters/');
  },
};
