import { createApp } from 'vue';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery';
import './registerServiceWorker';
import router from './router';
import store from './store';

// import vmodal from 'vue-js-modal'

// App.use(vmodal)
createApp(App).use(store).use(router).mount('#app');
